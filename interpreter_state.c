/*
  Lishp, a shell with lisp-like syntax written in C.
  
  Copyright (C) 2022 Miguel Ángel Martínez Quevedo
  
  This file is part of Lishp.

  Lishp is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

  Lishp is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with Lishp. If not, see <https://www.gnu.org/licenses/>. 
*/

#include "interpreter_state.h"

#include <stdio.h>

struct interpreter_state interpreter_state = {.should_quit = 0, .variable_map = NULL};

void
interpreter_state_init (char *envp[])
{
  static char key[1024+1];
  static char value[1024+1];
  interpreter_state.variable_map = variable_map_new ();
  for (char **var = envp;
       *var;
       var++)
    {
      int keylen = 0;
      int valuelen = 0;
      char *p = NULL;
      for (p = *var;
	   *p != '=';
	   p++)
	{
	  if (keylen == 1024)
	    {
	      fprintf (stderr,
		       "Max key length exceeded, "
		       "ignoring key-value pair.\n");
	      goto end;
	    }
	  key[keylen++] = *p;
	}
      key[keylen] = 0;
      for (p++;
	   *p;
	   p++)
	{
	  if (valuelen == 1024)
	    {
	      fprintf (stderr,
		       "Max key length exceeded, "
		       "ignoring key-value pair.\n");
	      goto end;
	    }
	  value[valuelen++] = *p;
	}
      value[valuelen] = 0;
      struct value val;
      val.type = VALUE_TYPE_STRING;
      val.as_stringz = malloc (valuelen+1);
      memcpy (val.as_stringz, value, valuelen+1);
      variable_map_set (interpreter_state.variable_map, key, val);
    end:;
    }
}
