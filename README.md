# LISHP

Lishp is a shell written in C with a syntax inspired in Lisp. It tries to work as a normal shell for common usage patterns but for more advanced features it provides lisp-like syntax.

## DEPENDENCIES

- GNU Readline
- GNU Make (build)

## COMPILATION

To compile, just run make:
	
	$ make

This will produce the lishp binary in the current directory. Optionally, you can install the binary in a system-wide binary path by executing:

	# make install

with root permissions, as it will copy the binary into /usr/bin/.

## RUNNING

To run, simply execute the produced binary, that is:

	./lishp

If you didn't install system-wide it, or:

	lishp

If you installed it system-wide.

## USAGE

After running the executable, you will be presented with the following message:
	
	Welcome to Lishp. A simple Lisp Shell.

As well as a prompt, after which you write your commands.

Commands have the form: 

	<command> <argument 1> <argument 2> ...

Where command is the command to execute, and the arguments are the arguments to be given to the command. The exact way in which this is evaluated depends on the type of command: 

- If command is a string or symbol that gives the name of a path, or the name of a system wide binary, the program referenced is to be executed. The arguments are first evaluated as expressions, and the result of each expression is then converted to a string to be passed to the program.

- If command is the name of a shell builtin, that builtin is executed. The evaluation of the arguments depends on the type of builtin.

- If command is the name of a user-defined variable, the variable is evaluated to the value stored in the given variable, and then this is executed per the previous rules.

## EXAMPLES

### Running a system program

	ls --color=auto

will list the current directory's contents, with colors indicating the file type.

### Evaluating an arithmetic expression

	+ 1 2 3

will compute the expression "1 + 2 + 3".

	+ 1 2 (* 2 3)
	
will compute the expression "1 + 2 + 2 * 3".

### Setting variables

	set "x" 12
	
will create a variable named x that stores the integer 12.

	print (* 2 x)
	
will print the value stored in x, doubled (Also showing the print builtin).

## Executing a pipeline

As in bash, it is possible to create and execute a process pipeline. But as for the other operations, we use a lisp-syntax to express it, like so:

    | (ls) (grep "builtin")

will print all files in the current working directory that contain "builtin" in their names.

## If control structure

To do different things under different conditions, lishp has the "if" control structure. It has the usual syntax as in other languages:

    if (>= x 0) (print x "is non-negative") (print x "is negative")

will print whether 'x' is non-negative or negative.

## While control structure

For iteration lishp only has the "while" control structure. As the "if" control structure, it has the usual syntax:

	set "x" 10
    while (> x 0) (print x) (set "x" (- x 1))

will print the numbers from 10 to 1, in that order.
