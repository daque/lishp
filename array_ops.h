#define ARRAY_MAP(dest, source, f, n)		\
  for (int i = 0; i < (n); i++)			\
    {						\
      (dest)[i] = f ((source)[i]);		\
    }
