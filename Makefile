
SOURCES=lishp.c lishp_evaluation.c value.c process.c variable_map.c interpreter_state.c builtin_base.c builtin_arithmetic.c sexp-reader/sexp.c sexp-reader/sexp_reader.c
OBJECTS=$(SOURCES:.c=.o)
CFLAGS=-I .
LDFLAGS=-lreadline

all: lishp .depend

lishp: $(OBJECTS)
	gcc -o lishp $(OBJECTS) $(LDFLAGS)

.depend: $(SOURCES)
	gcc -I . -MM $(SOURCES) > .depend 2> /dev/null

install: lishp
	cp -f lishp /usr/bin/lishp

uninstall:
	rm -f /usr/bin/lishp

clean:
	rm -f *o */*o .depend

-include .depend
