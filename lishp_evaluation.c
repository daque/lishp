/*
  Lishp, a shell with lisp-like syntax written in C.
  
  Copyright (C) 2022 Miguel Ángel Martínez Quevedo
  
  This file is part of Lishp.

  Lishp is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

  Lishp is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with Lishp. If not, see <https://www.gnu.org/licenses/>. 
*/

#include "lishp_evaluation.h"

#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>

#include <sexp-reader/sexp.h>

#include "value.h"
#include "process.h"
#include "variable_map.h"
#include "interpreter_state.h"
#include "array_ops.h"

void
eval_args (struct sexp *args, struct value **arg_values, int *no_arg_values)
{
  struct sexp *arg;
  int i;
  assert (*arg_values == NULL);
  assert (*no_arg_values == 0);
  for (arg = args;
       arg;
       arg = arg->next)
    {
      (*no_arg_values)++;
    }
  *arg_values = malloc (sizeof (struct value) * (*no_arg_values));
  for (arg = args, i = 0;
       arg;
       arg = arg->next)
    {
      (*arg_values)[i] = lishp_evaluate (arg);
      i++;
    }
}

char **
value_array_to_argv (char* first_argv, struct value *arg_values, int no_arg_values)
{
  no_arg_values++;
  char **argv = malloc (sizeof (char*) * (no_arg_values + 1));
  argv[0] = first_argv;
  ARRAY_MAP(argv+1, arg_values, value_stringify, no_arg_values-1);
  argv[no_arg_values] = NULL;
  return argv;
}

struct value
lishp_evaluate (struct sexp *sexp)
{
  if (!sexp) return (struct value){.type = NOT_A_VALUE};
  switch (sexp->type)
    {
    case SEXP_TYPE_LIST:
      {
	if (!sexp->list.front) return (struct value) {.type = NOT_A_VALUE};
	struct value first_value = lishp_evaluate (sexp->list.front);
	struct sexp *args = sexp->list.front->next;
	struct value *arg_values = NULL;
	int no_arg_values = 0;
	switch (first_value.type)
	  {
	  case VALUE_TYPE_STRING:
	    {
	      eval_args (args, &arg_values, &no_arg_values);
	      char **argv = value_array_to_argv (first_value.as_stringz,
						 arg_values,
						 no_arg_values);
	      struct process process = make_process (argv);
	      run_process (&process, STDIN_FILENO, STDOUT_FILENO);
	      wait_for_process (&process);
	      return (struct value) {.type = NOT_A_VALUE}; // TODO: return return code?
	    }
	    break;
	  case VALUE_TYPE_INT:
	    {
	      fprintf (stderr, "Cannot execute a number (%d).\n", first_value.as_int);
	      return (struct value) {.type = NOT_A_VALUE};
	    }
	    break;
	  case VALUE_TYPE_SPECIAL_FORM:
	    {
	      struct special_form special_form = first_value.as_special_form;
	      struct sexp *transformed_sexp = special_form.execute (sexp);
	      return lishp_evaluate (transformed_sexp);
	    }
	    break;
	  case VALUE_TYPE_PROCEDURE:
	    {
	      struct procedure procedure = first_value.as_procedure;
	      eval_args (args, &arg_values, &no_arg_values);
	      return procedure.execute (arg_values, no_arg_values);
	    }
	    break;
	  }
      }
      break;
    case SEXP_TYPE_ATOM:
      {
	char *str;
	if (*(sexp->atom.chars) == '"')
	  {
	    assert (sexp->atom.chars[sexp->atom.length-1] == '"');
	    str = malloc (sexp->atom.length-2 + 1);
	    memcpy (str, sexp->atom.chars+1, sexp->atom.length-2);
	    str[sexp->atom.length-2] = 0;
	    return (struct value) {.type = VALUE_TYPE_STRING, .as_stringz = str};
	  }
	str = malloc (sexp->atom.length + 1);
	memcpy (str, sexp->atom.chars, sexp->atom.length);
	str[sexp->atom.length] = 0;
	struct value value;
	if (variable_map_search (interpreter_state.variable_map, str, &value))
	  {
	    free (str);
	    return value;
	  }
	if (isdigit (*str)) // TODO: handle other cases
	  {
	    int as_int = atoi (str);
	    free (str);
	    return (struct value) {.type = VALUE_TYPE_INT, .as_int = as_int};
	  }
	return (struct value) {.type = VALUE_TYPE_STRING, .as_stringz = str};
      }
      break;
    }
}

