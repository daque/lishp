/*
  Lishp, a shell with lisp-like syntax written in C.
  
  Copyright (C) 2022 Miguel Ángel Martínez Quevedo
  
  This file is part of Lishp.

  Lishp is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

  Lishp is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with Lishp. If not, see <https://www.gnu.org/licenses/>. 
*/

#include "value.h"

#include <stdio.h>
#include <stdlib.h>

// TODO: This should always returned a newly allocated string, not the current mess.
char *
value_stringify (struct value value)
{
  char *str;
  switch (value.type)
    {
    case VALUE_TYPE_STRING:
      return value.as_stringz;
    case VALUE_TYPE_INT:
      str = malloc (20);
      snprintf (str, 20, "%d", value.as_int);
      return str;
    case VALUE_TYPE_SPECIAL_FORM:
      return "<special_form>";
    case VALUE_TYPE_PROCEDURE:
      return "<procedure>";
    case NOT_A_VALUE:
      return "<not-a-value>";
    }
}

int
value_to_bool (struct value value)
{
  if (value.type == VALUE_TYPE_INT && value.as_int == 0 ||
      value.type == NOT_A_VALUE) return 0;
  return 1;
}
