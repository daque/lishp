/*
  Lishp, a shell with lisp-like syntax written in C.
  
  Copyright (C) 2022 Miguel Ángel Martínez Quevedo
  
  This file is part of Lishp.

  Lishp is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

  Lishp is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with Lishp. If not, see <https://www.gnu.org/licenses/>. 
*/

#include <stdio.h>
#include <unistd.h>
#include <assert.h>
#include <ctype.h>
#include <stdlib.h>
#include <signal.h>

#include <sys/wait.h>

#include <readline/readline.h>
#include <readline/history.h>

#include <sexp-reader/sexp.h>
#include <sexp-reader/sexp_reader.h>

#include <string-map/string_map.h>

#include "value.h"
#include "variable_map.h"
#include "interpreter_state.h"
#include "builtin_arithmetic.h"
#include "builtin_base.h"
#include "process.h"
#include "lishp_evaluation.h"

char*
get_prompt ()
{
  static char prompt_buffer[64];
  char *cwd = getcwd (NULL, 0);
  if (cwd)
    {
      char *leaf = cwd + strlen (cwd);
      while (*leaf != '/' && leaf != cwd) leaf--;
      if (*leaf == '/') leaf++;
      snprintf (prompt_buffer, sizeof(prompt_buffer), "\1\e[94m\2%s > \1\e[0m\2", leaf);
      free (cwd);
    }
  else
    {
      strcpy (prompt_buffer, "??? > ");
    }
  return prompt_buffer;
}

ssize_t
lishp_read_line (char **line_ptr, size_t *line_length_ptr)
{
  if (*line_ptr) free (*line_ptr);
  *line_ptr = readline (get_prompt ());
  *line_length_ptr = strlen (*line_ptr);
  if (*line_ptr == NULL) return -1;
  add_history (*line_ptr);
  return 0;
}

int
main (int argc, char *argv[], char *envp[])
{

  // populate interpreter with builtins.
  interpreter_state_init (envp);
  interpreter_state_add_builtin_base ();
  interpreter_state_add_builtin_arithmetic ();

  // disable some signals to ignore C-c and alike.
  signal (SIGINT, SIG_IGN);
  signal (SIGQUIT, SIG_IGN);
  signal (SIGTSTP, SIG_IGN);

  // intro message and line declaration.
  puts ("Welcome to Lishp. A simple Lisp Shell.");
  char *line = NULL;
  size_t line_length = 0;

  // REPL main loop.
  while (lishp_read_line (&line, &line_length) != -1)
    {
      // read command
      struct sexp *command = sexp_reader_read_string (line);

      // execute command
      assert (command->type == SEXP_TYPE_LIST); // root node is always a list.
      struct value value = lishp_evaluate (command);

      // print command result
      if (value.type != NOT_A_VALUE)
	printf ("= %s\n", value_stringify (value));

      // cleanup
      sexp_destroy (command);

      // should we quit?
      if (interpreter_state.should_quit)
	{
	  puts ("Bye.");
	  break;
	}

    }

  free (line); // We will exit anyways, but better to be somewhat civilized.
  return 0;
}
