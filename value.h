/*
  Lishp, a shell with lisp-like syntax written in C.
  
  Copyright (C) 2022 Miguel Ángel Martínez Quevedo
  
  This file is part of Lishp.

  Lishp is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

  Lishp is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with Lishp. If not, see <https://www.gnu.org/licenses/>. 
*/

#pragma once

struct value;

enum value_type
  {
    VALUE_TYPE_STRING,
    VALUE_TYPE_INT,
    VALUE_TYPE_SPECIAL_FORM,
    VALUE_TYPE_PROCEDURE,
    NOT_A_VALUE
  };

struct special_form
{
  struct sexp* (*execute) (struct sexp*);
};

struct procedure
{
  struct value (*execute) (struct value*,int);
};

struct value
{
  enum value_type type;
  union
  {
    char *as_stringz;
    int as_int;
    struct special_form as_special_form;
    struct procedure as_procedure;
  };
};

char *
value_stringify (struct value value);

int
value_to_bool (struct value value);
