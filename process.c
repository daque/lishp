/*
  Lishp, a shell with lisp-like syntax written in C.
  
  Copyright (C) 2022 Miguel Ángel Martínez Quevedo
  
  This file is part of Lishp.

  Lishp is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

  Lishp is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with Lishp. If not, see <https://www.gnu.org/licenses/>. 
*/

#include "process.h"

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <signal.h>
#include <sys/resource.h>

struct process
make_process (char **argv)
{
  return (struct process) {.argv = argv, .started = 0, .pid = 0, .status = -1};
}

void
run_process (struct process *process, int read_from, int write_to)
{
  pid_t pid = fork ();
  if (pid == -1)
    {
      perror ("fork");
      return;
    }
  if (pid == 0)
    {
      signal (SIGPIPE, SIG_DFL);
      signal (SIGINT, SIG_DFL);
      signal (SIGQUIT, SIG_DFL);
      signal (SIGTSTP, SIG_DFL);
      dup2 (read_from, STDIN_FILENO);
      dup2 (write_to, STDOUT_FILENO);
      // TODO: ? don't be such a brute and only close actually open file descriptors.
      int fd_max;
      struct rlimit limit;
      getrlimit (RLIMIT_NOFILE, &limit);
      fd_max = limit.rlim_cur;
      for (int i = 3; i < fd_max; i++) close (i);
      execvp (process->argv[0], process->argv);
      fprintf (stderr, "Error while executing ");
      for (char **arg = process->argv;
	   *arg;
	   arg++)
	{
	  fprintf (stderr, "%s", *arg);
	  if (*(arg+1)) fprintf (stderr, " ");
	}
      perror (", execvp says");
      exit (1);
    }
  else
    {
      if (read_from != STDIN_FILENO) close (read_from);
      if (write_to != STDOUT_FILENO) close (write_to);
      process->pid = pid;
      process->started = 1;
      process->status = -1;
    }
}

#include <sys/wait.h>

void
wait_for_process (struct process *process)
{
  int wstatus;
  if (waitpid (process->pid, &wstatus, 0) == -1)
    {
      perror ("waitpid");
      return;
    }
  if (WIFEXITED(wstatus))
    {
      process->status = WEXITSTATUS(wstatus);
    }
}
