/*
  Lishp, a shell with lisp-like syntax written in C.
  
  Copyright (C) 2022 Miguel Ángel Martínez Quevedo
  
  This file is part of Lishp.

  Lishp is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

  Lishp is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with Lishp. If not, see <https://www.gnu.org/licenses/>. 
*/

#include "builtin_arithmetic.h"

#include "value.h"

static struct value
add (struct value *values, int no_values)
{
  int sum = 0;
  for (int i = 0; i < no_values; i++)
    {
      if (values[i].type != VALUE_TYPE_INT) return (struct value) {.type = NOT_A_VALUE};
      sum += values[i].as_int;
    }
  return (struct value) {.type = VALUE_TYPE_INT, .as_int = sum};
}

static struct value
multiply (struct value *values, int no_values)
{
  int product = 1;
  for (int i = 0; i < no_values; i++)
    {
      if (values[i].type != VALUE_TYPE_INT) return (struct value) {.type = NOT_A_VALUE};
      product *= values[i].as_int;
    }
  return (struct value) {.type = VALUE_TYPE_INT, .as_int = product};  
}

static struct value
divide (struct value *values, int no_values)
{
  if (!no_values) return (struct value) {.type = NOT_A_VALUE};
  int division = values[0].as_int;
  for (int i = 1; i < no_values; i++)
    {
      if (values[i].type != VALUE_TYPE_INT || values[i].as_int == 0) return (struct value) {.type = NOT_A_VALUE};
      division /= values[i].as_int;
    }
  return (struct value) {.type = VALUE_TYPE_INT, .as_int = division};  
}

static struct value
substract (struct value *values, int no_values)
{
  if (!no_values) return (struct value) {.type = NOT_A_VALUE};
  int substraction = values[0].as_int;
  for (int i = 1; i < no_values; i++)
    {
      if (values[i].type != VALUE_TYPE_INT) return (struct value) {.type = NOT_A_VALUE};
      substraction -= values[i].as_int;
    }
  return (struct value) {.type = VALUE_TYPE_INT, .as_int = substraction};
}

void
interpreter_state_add_builtin_arithmetic ()
{
  variable_map_set (interpreter_state.variable_map,
		    "+", (struct value) {.type = VALUE_TYPE_PROCEDURE,
			  .as_procedure = { .execute = add }});
  
  variable_map_set (interpreter_state.variable_map,
		    "-", (struct value) {.type = VALUE_TYPE_PROCEDURE,
			  .as_procedure = { .execute = substract }});
  
  variable_map_set (interpreter_state.variable_map,
		    "*", (struct value) {.type = VALUE_TYPE_PROCEDURE,
			  .as_procedure = { .execute = multiply }});
  
  variable_map_set (interpreter_state.variable_map,
		    "/", (struct value) {.type = VALUE_TYPE_PROCEDURE,
			  .as_procedure = { .execute = divide }});
}
