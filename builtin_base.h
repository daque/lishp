/*
  Lishp, a shell with lisp-like syntax written in C.
  
  Copyright (C) 2022 Miguel Ángel Martínez Quevedo
  
  This file is part of Lishp.

  Lishp is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

  Lishp is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with Lishp. If not, see <https://www.gnu.org/licenses/>. 
*/

#pragma once

#include "interpreter_state.h"

void
interpreter_state_add_builtin_base ();
