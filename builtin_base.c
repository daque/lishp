/*
  Lishp, a shell with lisp-like syntax written in C.
  
  Copyright (C) 2022 Miguel Ángel Martínez Quevedo
  
  This file is part of Lishp.

  Lishp is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

  Lishp is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with Lishp. If not, see <https://www.gnu.org/licenses/>. 
*/

#include "builtin_base.h"

#include <stdio.h>
#include <unistd.h>
#include <assert.h>
#include <sys/wait.h>
#include <string.h>
#include <stdlib.h>

#include <sexp-reader/sexp.h>

#include "value.h"
#include "process.h"
#include "lishp_evaluation.h"

static struct value
set (struct value *values, int no_values)
{
  if (no_values != 2 || values[0].type != VALUE_TYPE_STRING)		
    {									
      fprintf (stderr, "Usage: set <variable-name> <value>\n");	
      return (struct value) {.type = VALUE_TYPE_INT};			
    }									
  variable_map_set (interpreter_state.variable_map, values[0].as_stringz, values[1]); 
  return values[1];							
}

static struct value
print (struct value *values, int no_values)
{
  for (int i = 0; i < no_values; i++)
    {
      char *str = value_stringify (values[i]);
      printf ("%s ", str);
    }
  putchar ('\n');
  return (struct value) {.type = NOT_A_VALUE};
}

static struct value
cd (struct value *values, int no_values)
{
  struct value home_value;
  int has_home = variable_map_search (interpreter_state.variable_map, "HOME", &home_value);
  if (no_values > 1)
    {
      fprintf (stderr, "Usage: cd <directory to go>\n");
      return (struct value) {.type = NOT_A_VALUE};
    }
  assert (no_values >= 0 && no_values <= 1);
  if (no_values == 0)
    {
      if (!has_home || home_value.type != VALUE_TYPE_STRING)
	{
	  fprintf (stderr, "Valid HOME not present.\n");
	  return (struct value) {.type = NOT_A_VALUE};
	}
    }
  else // no_values == 1
    {
      assert (no_values == 1);
      if (values[0].type != VALUE_TYPE_STRING)
	{
	  fprintf (stderr, "Directory must be a string.\n");
	  return (struct value) {.type = NOT_A_VALUE};
	}
    }
  char *new_wd = no_values == 1? values[0].as_stringz : home_value.as_stringz;
  if (chdir (new_wd))
    {
      perror ("chdir");
      return (struct value) {.type = NOT_A_VALUE};
    }
  return (struct value) {.type = NOT_A_VALUE};
}

static struct value
quit (struct value *values, int no_values)
{
  interpreter_state.should_quit = 1;
  return (struct value) {.type = NOT_A_VALUE};
}

static struct sexp*
execute_pipe (struct sexp *sexp)
{
  assert (sexp->type == SEXP_TYPE_LIST);
  struct sexp *first = sexp->list.front;
  assert (first);
  int read_from = STDIN_FILENO;
  int write_to;
#define MAX_PROCESSES 1024
  // max number of processes validation.
  {
    int count = 0;
    for (struct sexp *process_sexp = first->next;
	 process_sexp;
	 process_sexp = process_sexp->next)
      count++;
    if (count > MAX_PROCESSES)
      {
	fprintf (stderr,
		 "Too many processes given in the pipe."
		 "Maximum is %d."
		 "%d were given.\n",
		 MAX_PROCESSES,
		 count);
	return NULL;
      }
  }
  struct process processes[MAX_PROCESSES];
  int no_processes = 0;
#undef MAX_PROCESSES
  // start launching processes.
  for (struct sexp *process_sexp = first->next;
       process_sexp;
       process_sexp = process_sexp->next)
    {
      int pipes[2];
      if (process_sexp->next)
	{
	  if (pipe (pipes) == -1)
	    {
	      perror ("pipe");
	      exit (1);
	    }
	  write_to = pipes[1];
	}
      else
	{
	  write_to = STDOUT_FILENO;
	}
      if (process_sexp->type != SEXP_TYPE_LIST ||
	  process_sexp->list.front == NULL)
	{
	  fprintf (stderr, "A non-process was supplied.\n");
	  return NULL;
	}
      struct value first_value = lishp_evaluate (process_sexp->list.front);
      if (first_value.type != VALUE_TYPE_STRING)
	{
	  fprintf (stderr, "A non-process was supplied.\n");
	  return NULL;
	}
      struct sexp *args = process_sexp->list.front->next;
      struct value *arg_values = NULL;
      int no_arg_values = 0;
      eval_args (args, &arg_values, &no_arg_values);
      char **argv = value_array_to_argv (first_value.as_stringz,
					 arg_values,
					 no_arg_values);
      struct process process = make_process (argv);
      run_process (&process, read_from, write_to);
      processes[no_processes++] = process;
      read_from = pipes[0];
    }
  // wait for every process to terminate.
  for (int i = 0; i < no_processes; i++)
    {
      pid_t pid;
      int found_matching = 0;
      while (!found_matching)
	{
	  int wstatus;
	  pid = waitpid (WAIT_ANY, &wstatus, 0);
	  // TODO: investigate if instead if retrying we should always just quit.
	  if (pid == -1) 
	    {
	      perror ("waitpid");
	      continue;
	    }
	  for (int j = 0; j < no_processes; j++)
	    {
	      if (processes[j].pid == pid)
		{
		  found_matching = 1;
		  if (WIFEXITED(wstatus))
		    processes[j].status = WEXITSTATUS(wstatus);
		  break;
		}
	    }
	}
    }
  return NULL;
}

static struct value
equals_func (struct value *values, int no_values)
{
  struct value value;
  value.type = VALUE_TYPE_INT;
  value.as_int = 1;
  switch (values[0].type)
    {
    case VALUE_TYPE_STRING:
      for (int i = 1; i < no_values; i++)
	{
	  if (values[i].type != VALUE_TYPE_STRING ||
	      strcmp (values[0].as_stringz, values[i].as_stringz))
	    {
	      value.as_int = 0;
	      break;
	    }
	}
      break;
    case VALUE_TYPE_INT:
      for (int i = 1; i < no_values; i++)
	{
	  if (values[i].type != VALUE_TYPE_INT ||
	      values[i].as_int != values[0].as_int)
	    {
	      value.as_int = 0;
	      break;
	    }
	}
      break;
    case VALUE_TYPE_SPECIAL_FORM:
      for (int i = 1; i < no_values; i++)
	{
	  if (values[i].type != VALUE_TYPE_SPECIAL_FORM ||
	      memcmp (&values[i].as_special_form,
		      &values[0].as_special_form,
		      sizeof(struct special_form)))
	    {
	      value.as_int = 0;
	      break;
	    }
	}
      break;
    case VALUE_TYPE_PROCEDURE:
      for (int i = 1; i < no_values; i++)
	{
	  if (values[i].type != VALUE_TYPE_PROCEDURE ||
	      memcmp (&values[i].as_procedure,
		      &values[0].as_procedure,
		      sizeof(struct procedure)))
	    {
	      value.as_int = 0;
	      break;
	    }
	}
      break;
    case NOT_A_VALUE:
      value.as_int = 0;
      break;
    }
  return value;
}

static struct value
lessthan_func (struct value *values, int no_values)
{
  struct value ans;
  ans.type = VALUE_TYPE_INT;
  ans.as_int = 1;
  if (values[0].type != VALUE_TYPE_INT)
    {
      ans.as_int = 0;
      return ans;
    }
  for (int i = 1; i < no_values; i++)
    {
      if (values[i].type != VALUE_TYPE_INT ||
	  values[i].as_int <= values[i-1].as_int)
	{
	  ans.as_int = 0;
	  break;
	}
    }
  return ans;
}

static struct value
greaterthan_func (struct value *values, int no_values)
{
  struct value ans;
  ans.type = VALUE_TYPE_INT;
  ans.as_int = 1;
  if (values[0].type != VALUE_TYPE_INT)
    {
      ans.as_int = 0;
      return ans;
    }
  for (int i = 1; i < no_values; i++)
    {
      if (values[i].type != VALUE_TYPE_INT ||
	  values[i].as_int >= values[i-1].as_int)
	{
	  ans.as_int = 0;
	  break;
	}
    }
  return ans;
}

static struct value
lessthanorequal_func (struct value *values, int no_values)
{
  struct value ans;
  ans.type = VALUE_TYPE_INT;
  ans.as_int = 1;
  if (values[0].type != VALUE_TYPE_INT)
    {
      ans.as_int = 0;
      return ans;
    }
  for (int i = 1; i < no_values; i++)
    {
      if (values[i].type != VALUE_TYPE_INT ||
	  values[i].as_int < values[i-1].as_int)
	{
	  ans.as_int = 0;
	  break;
	}
    }
  return ans;
}

static struct value
greaterthanorequal_func (struct value *values, int no_values)
{
  struct value ans;
  ans.type = VALUE_TYPE_INT;
  ans.as_int = 1;
  if (values[0].type != VALUE_TYPE_INT)
    {
      ans.as_int = 0;
      return ans;
    }
  for (int i = 1; i < no_values; i++)
    {
      if (values[i].type != VALUE_TYPE_INT ||
	  values[i].as_int > values[i-1].as_int)
	{
	  ans.as_int = 0;
	  break;
	}
    }
  return ans;
}

static struct sexp *
if_func (struct sexp *sexp)
{
  assert (sexp->type == SEXP_TYPE_LIST);
  assert (sexp->list.front);
  struct sexp *if_sym = sexp->list.front;
  struct sexp *condition = NULL, *then = NULL, *otherwise = NULL;
  condition = if_sym->next;
  if (condition) then = condition->next;
  if (then) otherwise = then->next;
  if (value_to_bool (lishp_evaluate (condition)))
    {
      return then;
    }
  return otherwise;
}

static struct sexp *
while_func (struct sexp *sexp)
{
  assert (sexp->type == SEXP_TYPE_LIST);
  assert (sexp->list.front);
  struct sexp *while_sym = sexp->list.front;
  struct sexp *condition = while_sym->next;
  if (!condition)
    {
      fprintf (stderr, "No condition given.\n");
      return NULL;
    }
  struct sexp *body = condition->next;
  while (value_to_bool (lishp_evaluate (condition)))
    {
      for (struct sexp *statement = body;
	   statement;
	   statement = statement->next)
	lishp_evaluate (statement);
    }
  return NULL; // TODO: return something more meaningful.
}

void
interpreter_state_add_builtin_base ()
{
  
  variable_map_set (interpreter_state.variable_map,
		    "set", (struct value) {.type = VALUE_TYPE_PROCEDURE,
			  .as_procedure = { .execute = set }});
  
  variable_map_set (interpreter_state.variable_map,
		    "print", (struct value) {.type = VALUE_TYPE_PROCEDURE,
			  .as_procedure = { .execute = print }});
  
  variable_map_set (interpreter_state.variable_map,
		    "cd", (struct value) {.type = VALUE_TYPE_PROCEDURE,
			  .as_procedure = { .execute = cd }});
  
  variable_map_set (interpreter_state.variable_map,
		    "quit", (struct value) {.type = VALUE_TYPE_PROCEDURE,
			  .as_procedure = { .execute = quit }});
  
  variable_map_set (interpreter_state.variable_map,
		    "|", (struct value) {.type = VALUE_TYPE_SPECIAL_FORM,
			  .as_special_form = { .execute = execute_pipe }});
  
  variable_map_set (interpreter_state.variable_map,
		    "if", (struct value) {.type = VALUE_TYPE_SPECIAL_FORM,
			  .as_special_form = { .execute = if_func }});
  
  variable_map_set (interpreter_state.variable_map,
		    "while", (struct value) {.type = VALUE_TYPE_SPECIAL_FORM,
			  .as_special_form = { .execute = while_func }});
  
  variable_map_set (interpreter_state.variable_map,
		    "=", (struct value) {.type = VALUE_TYPE_PROCEDURE,
			  .as_procedure = { .execute = equals_func }});
  
  variable_map_set (interpreter_state.variable_map,
		    "<", (struct value) {.type = VALUE_TYPE_PROCEDURE,
			  .as_procedure = { .execute = lessthan_func }});
  
  variable_map_set (interpreter_state.variable_map,
		    ">", (struct value) {.type = VALUE_TYPE_PROCEDURE,
			  .as_procedure = { .execute = greaterthan_func }});
  
  variable_map_set (interpreter_state.variable_map,
		    "<=", (struct value) {.type = VALUE_TYPE_PROCEDURE,
			  .as_procedure = { .execute = lessthanorequal_func }});
  
  variable_map_set (interpreter_state.variable_map,
		    ">=", (struct value) {.type = VALUE_TYPE_PROCEDURE,
			  .as_procedure = { .execute = greaterthanorequal_func }});
}
